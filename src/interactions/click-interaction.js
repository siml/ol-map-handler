import common from "../lib/common/dist/common.min";

/**
 * Constructor. Interactions are disabled by default. To activate see {@link ClickInteraction#activate} 
 * function.
 * @class
 * @classdesc A class for simplifying click-query interactions on the map. Three types of queries are handled:
 * direct queries on vector features on the map, querying via an AJAX web call, and querying a WMS getFeatures
 * service. On returning the features (or whatever data is returned by the web services), the interaction can 
 * process the resultant data and return HTML for a map popup.
 * @constructor
 * @param {MapHandler} mapHandler
 * @returns {ClickInteraction}
 */
function ClickInteraction(mapHandler) {
    this.mapHandler           = mapHandler;
    this.olMap                = this.mapHandler.olMap;
    this._isActive            = false;
    this._clickListenerBound  = this._clickListener.bind(this);
    this._before              = null;
    this._after               = null;
    this.featureInteractions  = {};
    this.ajaxInteractions     = {};
    this.wmsInteractions      = {};
    this.wmsRequestProxy      = null;
}


/**
 * Activate interactions. Must be called if to be used, as by default, interactions are disabled.
 * @memberof ClickInteraction
 */
ClickInteraction.prototype.activate = function() {
    if(!this._isActive) {
        this.olMap.on('click', this._clickListenerBound);
        this._isActive = true;
    }
};

/**
 * Disable interactions.
 * @memberof ClickInteraction
 */
ClickInteraction.prototype.deactivate = function() {
    if(this._isActive) {
        this.olMap.un('click', this._clickListenerBound);
        this._isActive = false;
    }
};

/**
 * Callback to run before starting click event.
 * @param {callback} callback or null
 * @memberof ClickInteraction
 */
ClickInteraction.prototype.before = function(callback) {
    this._before = callback;
};

/**
 * Callback to run after completing click event.
 * @param {callback} callback or null
 * @memberof ClickInteraction
 */
ClickInteraction.prototype.after = function(callback) {
    this._after = callback;
};


//************************************************************************************************************
// Interaction definitions
//************************************************************************************************************
/**
 * Definitions for interactions.
 * @typedef InteractionDefn
 * @param {LayerGroup[]} options.layers - List of layer objects (see {@link Overlays#LayerGroup}) for which 
 *        this interaction applies.
 * @param {string} [options.url] - URL of AJAX request to query data. Only used by AJAX query.
 * @param {string} [options.method="GET"] - Ajax method (POST or GET). Only used by AJAX query.
 * @param {string} [options.dataType] - Data type of returned data. Only used by AJAX query.
 * @param {CheckCallback} [options.check] - Callback to check (for individual {@link Overlays#LayerGroup}), 
 *        whether to include said {@link Overlays#LayerGroup} in a click query action.
 * @param {ToDataCallback} options.toData - Callback to create `data` paramter to pass in AJAX call.
 * @param {ErrorCallback} [options.error] - Callback to run on error.
 * @param {ClickCallback} [options.click] - Callback to run on each successful interaction.
 * @param {CompleteCallback} [options.complete] - Callback once complete.
 */
/**
 * Callback run for each {@link Overlays#LayerGroup} attached to this interaction. Optional -- if not supplied
 * all layers are assumed included.
 * @callback CheckCallback
 * @param {ol.Layer} layer - The layer object being checked for query.
 * @returns {boolean} Whether to include in query.
 */
/**
 * Callback to create data to pass to AJAX request based on either/or/none-of the coordinates and valid layers
 * (as filtered by {@link CheckCallback}) of this click query action. Only used by the AJAX query interaction.
 * @callback ToDataCallback
 * @param {number[]} coordinates - Coordinates of click query request.
 * @param {LayerGroup[]} layers[] - {@link Overlays#LayerGroup} of LayerGroup to check.
 * @returns {Object} Data to send with AJAX request.
 */
/**
 * Callback on error. Never triggered by feature query.
 * @callback ErrorCallback
 * @param {PassData} passData - Passable data. Assign or pull data from here as helpful.
 * @param jqXHR
 * @param textStatus
 * @param errorThrown
 */
/**
 * Callback on successful retrieval of each data element.
 * @callback ClickCallback
 * @param {PassData} passData - Passable data. Assign or pull data from here as helpful.
 * @param data - The data returned. May be array of WMS results, query results, or ol.Features.
 */
/**
 * Callback on complete.
 * @callback CompleteCallback
 * @param {PassData} passData - Passable data. Assign or pull data from here as helpful.
 * @param {Element} popupElem - The popup element.
 */
/**
 * Basic data object to allowing passing of data between promise callbacks on AJAX request. Values vary 
 * depending on interaction type. Mostly useful for adding data during each click callback to process when 
 * done in {@link ClickCallback}.
 * @typedef PassData
 */


//************************************************************************************************************
// Adding/removing interactions
//************************************************************************************************************
ClickInteraction.prototype.addFeatureQuery = function(key, options) {
    this.featureInteractions[key] = options;
};

ClickInteraction.prototype.removeFeatureQuery = function(key) {
    if(key in this.ajaxInteractions) {
        this.featureInteractions[key] = null;
        delete this.featureInteractions[key];
    }
};

/**
 * Add basic click interaction.
 * @memberof ClickInteraction
 * @param {string} key - Unique identifier for this interaction
 * @param {InteractionDefn} options
 */
ClickInteraction.prototype.addAjaxQuery = function(key, options) {
    this.ajaxInteractions[key] = options;
};

/**
 * Remove basic click interaction.
 * @memberof ClickInteraction
 * @param {string} key - Unique identifier for interaction to delete.
 */
ClickInteraction.prototype.removeAjaxQuery = function(key) {
    if(key in this.ajaxInteractions) {
        this.ajaxInteractions[key] = null;
        delete this.ajaxInteractions[key];
    }
};

/**
 * Add WMS click functionality. Note that adding is a two step process. First call this to add an interaction,
 * then call applyWMSQueryToLayers to apply the given interaction to layers.
 * @memberof ClickInteraction
 * @param {string} key - Unique type name to give to this set of interactions.
 * @param {InteractionDefn} options
 */
ClickInteraction.prototype.addWMSQuery = function(key, options) {
    this.wmsInteractions[key] = options;
};

/**
 * Remove click functionalities for given type.
 * @memberof ClickInteraction
 * @param {string} key - Unique type name of interactions to remove.
 * @param {ol.layer|ol.layer[]} layers- Either singular or array of OpenLayers layer objects to remove WMS
 *        interactions from.
 * @returns {Object} Literal of interactions being removed.
 */
ClickInteraction.prototype.removeWMSQuery = function(key) {
    if(key in this.wmsInteractions) {
        this.wmsInteractions[key] = null;
        delete this.wmsInteractions[key];
    }
};

ClickInteraction.prototype.addAliasForTMS = function(theTmsLayer, theWmsSource) {
    theTmsLayer.set("WmsClickAlias", theWmsSource);
};

ClickInteraction.prototype.removeAliasForTMS = function(theTmsLayer) {
    theTmsLayer.set("WmsClickAlias", false);
};

/**
 * Callback encapsulating WMS request proxy.
 * @callback onWmsRequest
 * @param {Object} options - Ajax request options (as would be fed to $.ajax). Modify then send as Ajax or 
 *        XMLHttpRequest.
 */
/**
 * Add a WMS proxy. Useful if necessary to bypass CORS issues.
 * @memberof ClickInteraction
 * @param {onWmsRequest} callback 
 */
ClickInteraction.prototype.setWMSRequestProxy = function(callback) {
    this.wmsRequestProxy = callback;
};



//************************************************************************************************************
// Click listener
//************************************************************************************************************
ClickInteraction.prototype._clickListener = function(evt) {
    var self = this, 
        pixel = this.olMap.getEventPixel(evt.originalEvent), 
        coords = this.olMap.getEventCoordinate(evt.originalEvent), 
        results = [], 
        waitCount = 0, 
        asyncWait = 1, // start with 1 for check complete at end of this function
        onCompletes = [];
    var checkComplete = function() {
        if(++waitCount === asyncWait) {
            self._clickComplete(coords, "At this location", results, onCompletes);
        }
    };
    
    if(this._before) this._before();
    
    try {
        // click interactions by WMS get feature requests 
        if(Object.keys(this.wmsInteractions).length) {
            // this got too long so split into separate function
            if(this.__handleWMSRequests(coords, results, onCompletes, checkComplete)) {
                // only 1 wait check as all WMS requests separately handled for group finish
                ++asyncWait;
            }
        }

        // click interaction via generic web query
        if(Object.keys(this.ajaxInteractions).length) {
            // here requests are spawned by interaction, thus interaction handlers can be tied to each request
            // request parameters are formatted/read by the interaction options, as formats can vary
            var queryRequests = [];
            for(var qKey in this.ajaxInteractions) {
                var interaction = this.ajaxInteractions[qKey], 
                    queryLayers = [];
                for(var i = 0; i < interaction.layers.length; ++i) {
                    if(!interaction.check || interaction.check(interaction.layers[i])) {
                        queryLayers.push(interaction.layers[i]);
                    }
                }
                if(queryLayers.length) {
                    ++asyncWait;
                    var passData = {params: interaction.toData(coords, queryLayers)};
                    if(interaction.complete) {
                        onCompletes.push(function(elem) {
                            interaction.complete(passData, elem);
                        });
                    }
                    queryRequests.push({
                        url: interaction.url, 
                        method: interaction.method || "GET", 
                        data: passData.params, 
                        dataType: interaction.dataType || undefined, 
                        error: function(jqXHR, textStatus, errorThrown) {
                            if(interaction.error) {
                                results.push(interaction.error(passData, jqXHR, textStatus, errorThrown));
                            }
                            console.error(jqXHR);
                        }, 
                        success: function(res) {
                            if(interaction.click) {
                                results.push(interaction.click(passData, res));
                            }
                        }, 
                        complete: checkComplete
                    });
                }
            }
            for(var i = 0; i < queryRequests.length; ++i) {
                common.ajax(queryRequests[i]);
            }
        }

        // click interactions by features (done last after ajax results are spawned)
        if(Object.keys(this.featureInteractions).length) {
            // here requests are spawned by feature, then at finish grouped by interaction
            // no actual request, we simply grab every feature that qualifies and return it to interaction
            var passDataByInteraction = {};
            this.olMap.forEachFeatureAtPixel(
                pixel,
                function(feature) {
                    for(var key in this.clickInteractions) {
                        interaction = this.clickInteractions[key];
                        for(var i = 0; i < interaction.layers.length; ++i) {
                            var layer = interaction.layers[i];
                            if(interaction.check && !interaction.check(layer)) continue;
                            if(!layer.getSource().hasFeature(feature)) return;
                            // feature part of interaction
                            if(!(interaction in passDataByInteraction)) {
                                passDataByInteraction[interaction] = {features: []};
                            }
                            // add feature
                            if(!interaction.check || interaction.check(interaction.layers[i])) {
                                passDataByInteraction[interaction].features.push(feature);
                            }
                        }
                    }
                }
            );
            for(var interaction in passDataByInteraction) {
                var passData = passDataByInteraction[interaction];
                // query and push popup data
                if(interaction.click) {
                    results.push(interaction.click(passData, passData.features));
                }
                // add on complete
                if(interaction.complete) {
                    onCompletes.push(function(elem) {
                        interaction.complete(passData, elem);
                    });
                }
            }
        }

        // always final check complete
        checkComplete();
        
    } catch(e) {
        console.error(e);
        if(this._after) this._after();
    }
};

ClickInteraction.prototype._clickComplete = function(coords, title, results, onCompletes) {
    var complete = function(popupElem) {
        for(var i = 0; i < onCompletes.length; ++i) {
            onCompletes[i](popupElem);
        }
    };
    if(this._after) this._after();
    if(!results || !results.length) {
        this.mapHandler.interactions.hideMarker();
        this.mapHandler.interactions.closePopup();
        complete();
        return;
    }
    var html = title ? "<h3 class='title'>"+title+"</h3>" : "";
    for(var i = 0; i < results.length; ++i) {
        if(results[i]) html += results[i];
    }
    if(html === "") {
        this.mapHandler.interactions.hideMarker();
        this.mapHandler.interactions.closePopup();
        complete();
        return;
    }
    this.mapHandler.interactions.showMarker(coords);
    this.mapHandler.interactions.openPopup(coords, html, null, complete);
};

ClickInteraction.prototype.__handleWMSRequests = function(coords, results, onCompletes, checkComplete) {
    // here all requests are spawned by layer, then wait till all finish to group by interactions
    // requests are fed the WMS parameters and return the parsed GML feature data
    // to avoid doubling up the same request if a layer is present in more than one interaction, use 
    // below maps to simplify after
    var allRequests = [], 
        requestByLayer = {}, 
        interactionByLayer = {}, 
        resolution = this.olMap.getView().getResolution();
    for(var key in this.wmsInteractions) {
        var interaction = this.wmsInteractions[key];
        for(var i = 0; i < interaction.layers.length; ++i) {
            var layer = interaction.layers[i];
            if(!interaction.check || interaction.check(interaction.layers[i])) {
                // create request only if not already existing for layer
                if(!(layer in requestByLayer)) {
                    var source = layer.get("WmsClickAlias") || layer.getSource(), 
                        params = source.getParams();
                    if(!params.layers) continue;
                    requestByLayer[layer] = source.getGetFeatureInfoUrl(
                        coords, 
                        resolution, 
                        this.mapHandler.epsgWebMercator, 
                        {"QUERY_LAYERS": params.layers, "INFO_FORMAT": "gml"}
                    );
                    interactionByLayer[layer] = [];
                    allRequests.push(requestByLayer[layer]);
                }
                // add to map by interactions
                interactionByLayer[layer].push(key);
            }
        }
    }
    if(!allRequests.length) return false;
    
    var parser = new DOMParser(), 
        responses = {}, 
        count = 0;
    for(var layer in requestByLayer) {
        // IIFE due to looping var scope
        var self = this, 
            options = (function(theLayer) {
                return {
                    url: requestByLayer[theLayer], 
                    error: function(jqXHR, textStatus, errorThrown) {
                        responses[theLayer] = {
                            error: {
                                jqXHR:jqXHR, 
                                textStatus: textStatus, 
                                errorThrown: errorThrown
                            }
                        };
                    }, 
                    success: function(res) {
                        var xml = parser.parseFromString(res, "text/xml");
                        // check if actually error first
                        var exception = self.__getServiceError(xml);
                        if(exception) {
                            responses[theLayer] = {
                                error: {
                                    jqXHR: res, 
                                    textStatus: "ServiceException", 
                                    errorThrown: exception.textContent
                                }
                            };
                        } else {
                            responses[theLayer] = {xml: xml};
                        }
                    }, 
                    complete: function() {
                        if(++count === allRequests.length) {
                            self.__handleWMSComplete(responses, results, interactionByLayer, onCompletes);
                            checkComplete();
                        }
                    }
                };
            })(layer);
        // send request
        if(!this.wmsRequestProxy) {
            common.ajax(options);
        } else {
            this.wmsRequestProxy(options);
        }
    }
    return true;
};

ClickInteraction.prototype.__handleWMSComplete = function(responses, results, interactionByLayer, onCompletes) {
    // create pass data objects
    var passDataByInteraction = {};
    for(var key in this.wmsInteractions) {
        passDataByInteraction[key] = {};
    }
    // start with error processing
    for(var layer in responses) {
        if(!responses[layer].error) continue;
        console.error(responses[layer].error.jqXHR);
        for(var i = 0; i < interactionByLayer[layer].length; ++i) {
            var iKey = interactionByLayer[layer][i], 
                interaction = this.wmsInteractions[iKey];
            if(interaction.error) {
                interaction.error(
                    passDataByInteraction[iKey], 
                    responses[layer].error.jqXHR, 
                    responses[layer].error.textStatus, 
                    responses[layer].error.errorThrown
                );
            }
            responses[layer] = null;
            delete responses[layer];
        }
    }
    // parse features and remap by interaction
    var featureMap = this.__parseWMSResponse(responses, interactionByLayer);
    // for active interactions, trigger callbacks
    for(var interactionKey in featureMap) {
        var interaction = this.wmsInteractions[interactionKey], 
            passData = passDataByInteraction[interactionKey];
        if(interaction.click) {
            results.push(interaction.click(passData, featureMap[interactionKey]));
        }
        if(interaction.complete) {
            onCompletes.push(function(elem) {
                interaction.complete(passData, elem);
            });
        }
    }
};

ClickInteraction.prototype.__parseWMSResponse = function(responses, interactionByLayer) {
    if(!responses) return {};
    // first parse responses
    var featData = [];
    for(var layer in responses) {
        var gml = this.__getGmlStart(responses[layer].xml);
        if(!gml) continue;
        // for each response check for feature data
        var children = gml.children || gml.childNodes;
        for(var j = 0; j < children.length; ++j) {
            var gmlLayer = children[j];
            if(typeof gmlLayer.tagName === "undefined" || !gmlLayer.tagName.endsWith("_layer")) continue;
            var layerChildren = gmlLayer.children || gmlLayer.childNodes;
            for(var k = 0; k < layerChildren.length; ++k) {
                if(typeof layerChildren[k].tagName === "undefined" || !layerChildren[k].tagName.endsWith("_feature")) continue;
                // parse feature and replace
                var feat = this.__serializeGmlNode(layerChildren[k], {}, true);
                feat._layer = layer;
                featData.push(feat);
            }
        }
    }
    // group feature data by all related interactions
    var responseMap = {};
    for(var f = 0; f < featData.length; ++f) {
        var feat = featData[f], 
            toInteractions = interactionByLayer[feat._layer];
        if(!toInteractions) continue;
        for(var i = 0; i < toInteractions.length; ++i) {
            if(!(toInteractions[i] in responseMap)) {
                responseMap[toInteractions[i]] = [];
            }
            responseMap[toInteractions[i]].push(feat);
        }
    }
    return responseMap;
};

ClickInteraction.prototype.__getServiceError = function(dom) {
    return this.__getXMLStart(dom, "ServiceException");
};

ClickInteraction.prototype.__getGmlStart = function(dom) {
    return this.__getXMLStart(dom, "msGMLOutput");
};

ClickInteraction.prototype.__getXMLStart = function(dom, tagName) {
    if(!dom) return;
    if(dom.tagName === tagName) return dom;
    var children = dom.children || dom.childNodes;
    if(!children) return null;
    for(var i = 0; i < children.length; ++i) {
        var gml = this.__getXMLStart(children[i], tagName);
        if(gml) return gml;
    }
};

ClickInteraction.prototype.__serializeGmlNode = function(gml, obj, first) {
    var children = gml.children || gml.childNodes;
    if(typeof gml.tagName === "undefined") return;
                           // I hate IE
    if(!children.length || (children.length === 1 && typeof children[0].tagName === "undefined")) {
        obj[gml.tagName] = gml.innerHTML || gml.textContent;
        return;
    }
    var subObj = obj;
    if(!first) {
        obj[gml.tagName] = subObj = {};
    }
    for(var i = 0; i < children.length; i++) {
        this.__serializeGmlNode(children[i], subObj, false);
    }
    return obj;
};

export {ClickInteraction as default};