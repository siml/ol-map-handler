function HoverInteraction(mapHandler) {
    this.mapHandler         = mapHandler;
    this.olMap              = this.mapHandler.olMap;
    this._isActive          = false;
    this.hoverInteractions  = {};
    this.tooltipElem        = null;
    this._isShowing         = false;
    this._moveListenerBound = this._moveListener.bind(this);
    
    
    // create tooltip element
    this.tooltipElem = document.createElement("div");
    this.tooltipElem.id = "ol-mh-tooltip";
    this.tooltipElem.style["position"] = "absolute";
    this.tooltipElem.style["visibility"] = "hidden";
    this.mapHandler.mapViewElem.append(this.tooltipElem);
}


/**
 * Activate interactions. Must be called if to be used, as by default, interactions are disabled.
 * @memberof HoverInteraction
 */
HoverInteraction.prototype.activate  = function() {
    if(!this._isActive) {
        this.olMap.on('mousemove', this._moveListenerBound);
        this._isActive = true;
    }
};

/**
 * Disable interactions.
 * @memberof HoverInteraction
 */
HoverInteraction.prototype.deactivate = function() {
    if(this._isActive) {
        this.olMap.un('mousemove', this._moveListenerBound);
        this._isActive = false;
    }
};

/**
 * Add hover interaction.
 * @param {string} key - Unique identifier for interaction..
 * @param {object} options - Definitions for interactions.
 * @param {LayerGroup[]} options.layers - List of layer objects (see {@link Overlays#LayerGroup}) for which 
 *        this interaction applies.
 * @param {callback} [options.check]
 * @param {callback} [options.clickable]
 * @param {callback} [options.hover]
 * @param {callback} [options.tooltip]
 * @memberof HoverInteraction
 */
HoverInteraction.prototype.add = function(key, options) {
    this.hoverInteractions[key] = options;
};

/**
 * Remove hover interaction.
 * @memberof ClickInteraction
 * @param {string} key - Unique identifier for interaction to delete.
 */
HoverInteraction.prototype.remove = function(key) {
    if(key in this.hoverInteractions) {
        this.hoverInteractions[key] = null;
        delete this.hoverInteractions[key];
    }
};

HoverInteraction.prototype._moveListener = function(evt) {
    if(!this._isActive) {
        if(!this._isShowing) {
            this.tooltipElem.style["visibility"] = "hidden";
            this._isShowing = false;
        }
        return;
    }
    
    var pixel              = this.olMap.getEventPixel(evt.originalEvent || evt), 
        matchedFeature     = null, 
        matchedInteraction = null;
    this.olMap.forEachFeatureAtPixel(
        pixel,
        function(feature) {
            if(!matchedFeature) {
                for(var hkey in this.hoverInteractions) {
                    var interaction = this.hoverInteractions[hkey];
                    for(var j = 0; j < interaction.layers; ++j) {
                        if(interaction.layers[j].getSource().hasFeature(matchedFeature)) {
                            if(!interaction.check || interaction.check(feature)) {
                                matchedFeature = feature;
                                matchedInteraction = interaction;
                                return true;
                            }
                        }
                    }
                }
            }
        }
    );
    if(matchedFeature) {
        if(matchedInteraction.clickable && matchedInteraction.clickable(matchedFeature)) {
            this.mapViewElem.css("cursor", "pointer");
        } else {
            this.mapViewElem.css("cursor", "");
        }
        if(matchedInteraction.hover) {
            matchedInteraction.hover(matchedFeature);
        }
        if(matchedInteraction.tooltip) {
            this.tooltipElem.innerHTML = matchedInteraction.tooltip(matchedFeature);
            this.tooltipElem.css({top: pixel[1]-10, left: pixel[0]+15});
            this.tooltipElem.style["visibility"] = "";
            this._isShowing = true;
        } else {
            this.tooltipElem.style["visibility"] = "hidden";
            this._isShowing = false;
        }
    } else {
        this.mapViewElem.css("cursor", "");
        this.tooltipElem.style["visibility"] = "hidden";
        this._isShowing = false;
    }
};

export {HoverInteraction as default};