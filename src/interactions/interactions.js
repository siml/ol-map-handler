import ClickInteraction from "./click-interaction.js";
import HoverInteraction from "./hover-interaction.js";

function Interactions(mapHandler, interactionHandler) {
    this.mapHandler = mapHandler;
    this.olMap = this.mapHandler.olMap;
    this.interactionHandler = interactionHandler;
    
    // for dragzoom
    this.dragZoomAlways = null;
    this.dragZoomShift  = null;
    // for popup
    this.popupIdPrefix  = "ol-mh-popup";
    this.popupOverlay   = null; 
    this.popupContainer = null;
    this.popupOverlay   = null;
    // for marker
    this.markerLayer    = null;
    this.markerFeature  = null;
    
    this.click = new ClickInteraction(this.mapHandler);
    this.hover = new HoverInteraction(this.mapHandler);
}

Interactions.prototype._init = function() {
    
    // custom drag zoom event for tool
    this.dragZoomAlways = new ol.interaction.DragZoom({
        condition: ol.events.condition.always
    });
    // elements tied to drag zoom
    this.dragZoomElems = [];
    // on ending dragbox, auto exit interaction not applied here but in bindMapControls();
    // add to map
    this.dragZoomAlways.setActive(false);
    this.olMap.addInteraction(this.dragZoomAlways);
    // shift key drag zoom
    this.dragZoomShift = new ol.interaction.DragZoom({
        condition: ol.events.condition.shiftKeyOnly
    });
    this.dragZoomShift.setActive(true);
    this.olMap.addInteraction(this.dragZoomShift);
    
    // marker
    this.markerFeature = new ol.Feature({
        geometry: new ol.geom.Point([0,0])
    });
    this.markerLayer = new ol.layer.Vector({
        source: new ol.source.Vector({
            features: [this.markerFeature]
        })
    });
    this.markerLayer.setVisible(false);
    this.markerLayer.setZIndex(9999);
    this.olMap.addLayer(this.markerLayer);
    // marker disappears on click
    this.olMap.on("click", this.hideMarker.bind(this));

    // create popup container and overlay
    var self = this;
    this.popupContainer = document.createElement("div");
    this.popupContainer.className = this.popupIdPrefix+"-container";
    this.mapHandler.mapViewElem.append(this.popupContainer);
    this.popupOverlay = new ol.Overlay({
        element: this.popupContainer,
        autoPan: true,
        autoPanAnimation: {duration: 250}
    });
    this.olMap.addOverlay(this.popupOverlay);
    // add closer 'X' to popup container
    var a = document.createElement("a");
    a.className = this.popupIdPrefix+"-closer";
    a.setAttribute('href', '#');
    a.innerHTML = "&#x1f5d9";
    this.popupContainer.append(a);
    a.addEventListener('click', function(evt) {
        evt.preventDefault();
        self.popupOverlay.setPosition(undefined);
        this.blur();
    });
    // popup content
    this.popupElem = document.createElement("div");
    this.popupElem.className = this.popupIdPrefix+"-content";
    this.popupContainer.append(this.popupElem);
    
    // listener on mousemove (for hover and pointer on clickable features)
    this.mapHandler.mapViewElem.addEventListener("mousemove", function(evt) {
    });
};


//************************************************************************************************************
// Zoom interactions
//************************************************************************************************************
/**
 * Bind some common map controls.
 * @param {InteractionHandler} interactionHandler - The interaction handler.
 * @param {Object} bindings - Map of bindings where key name is interaction type and value is jQuery selection
 *        of element to bind it to. Valid/recognized interactions are 'zoomIn', 'zoomOut', 'zoomExtent', and 
 *        'zoomBox'. Elements will have an attribute called 'interaction' attached with the interaction name.
 */
Interactions.prototype.bindMapControls = function(interactionHandler, bindings) {
    interactionHandler = interactionHandler || this.interactionHandler;
    var self = this, 
        elems = [];
    if(bindings.zoomIn) {
        interactionHandler.addInteraction("zoomIn", {
            start: function(evt) {
                var view = self.olMap.getView(), 
                    zoom = view.getZoom();
                view.setZoom(++zoom < view.getMaxZoom() ? zoom : --zoom);
                // immediately end interaction
                interactionHandler.endInteraction();
            }, 
            end: function() { }
        });
        bindings.zoomIn.setAttribute('ol-interaction', 'zoomIn');
        elems.append(bindings.zoomIn);
    }
    if(bindings.zoomOut) {
        interactionHandler.addInteraction("zoomOut", {
            start: function(evt) {
                var view = self.olMap.getView(), 
                    zoom = view.getZoom();
                view.setZoom(++zoom < view.getMaxZoom() ? zoom : ++zoom);
                // immediately end interaction
                interactionHandler.endInteraction();
            }, 
            end: function() { }
        });
        bindings.zoomOut.setAttribute('ol-interaction', 'zoomOut');
        elems.append(bindings.zoomOut);
    }
    if(bindings.zoomExtent) {
        interactionHandler.addInteraction("zoomExtent", {
            start: function(evt) {
                if(self.extent) {
                    self.olMap.getView().fit(self.extent);
                }
                // immediately end interaction
                interactionHandler.endInteraction();
            }, 
            end: function() { }
        });
        bindings.zoomExtent.setAttribute('ol-interaction', 'zoomExtent');
        elems.append(bindings.zoomExtent);
    }
    if(bindings.zoomBox) {
        // check if element is new
        var elem = $(bindings.zoomBox), 
            add = true;
        for(var i = 0; i < this.dragZoomElems.length; i++) {
            if(elem.is(this.dragZoomElems[i])) {
                add = false;
                break;
            }
        }
        // if so add to list of elements
        if(add) this.dragZoomElems.push(elem);
        // add binding
        interactionHandler.addInteraction("zoomBox", {
            start: function(evt) {
                var active = !self.dragZoomAlways.getActive();
                if(active) {
                    self.setDragZoom(active);
                } else {
                    interactionHandler.endInteraction();
                }
            }, 
            end: function() { self.setDragZoom(false); }
        });
        // add end interaction listener here (this lets it just stack w/o having to store interactionHandler)
        this.dragZoomAlways.on("boxend", function(evt) {
            interactionHandler.endInteraction();
        });
        bindings.zoomBox.setAttribute('ol-interaction', 'zoomBox');
        elems.append(bindings.zoomBox);
    }
    if(elems.length) {
        interactionHandler.bindUiElements(
            elems, 
            {
                event: 'click', 
                valueFunction: function() { return this.getAttribute("ol-interaction"); }
            }
        );
    }
};

/**
 * Turn on/off drag-zoom mode. Does the inverse for the default shift-drag interaction so they do not 
 * conflict.
 * @memberof EAMap
 * @param {Boolean} active
 */
Interactions.prototype.setDragZoom = function(active) {
    this.dragZoomShift.setActive(!active);
    this.dragZoomAlways.setActive(active);
    for(var i = 0; i < this.dragZoomElems.length; i++) {
        if(active) {
            this.dragZoomElems[i].addClass("active");
        } else {
            this.dragZoomElems[i].removeClass("active");
        }
    }
};

/**
 * Turn on/off shift+drag interaction for drag zoom.
 * @memberof EAMap
 * @param {Boolean} active
 */
Interactions.prototype.setShiftDragZoom = function(active) {
    this.dragZoomShift.setActive(active);
};

//************************************************************************************************************
// Popup
//************************************************************************************************************
/**
 * Open map popup.
 * @param {Number[]} coordinates - Map coordinates to open popup on.
 * @param {String} contentHtml - HTML content for popup.
 * @param {Object} [styles] - Optional CSS styles to apply to popup container.
 * @param {Callback} [onComplete] - Optional on complete callback. Passed popup element as parameter.
 */
Interactions.prototype.openPopup = function(coordinates, contentHtml, styles, onComplete) {
    this.popupOverlay.setPosition(coordinates);
    this.popupElem.innerHTML = contentHtml;
    this.popupContainer.setAttribute("style", "");
    if(styles) {
        for(var key in styles) {
            this.popupContainer.style[key] = styles[key];
        }
    }
    if(onComplete) onComplete(this.popupElem);
};

/**
 * Close map popup element, if open.
 */
Interactions.prototype.closePopup = function() {
    this.popupContainer.setAttribute("style", "");
    this.popupOverlay.setPosition(undefined);
    this.popupElem.blur();
};

//************************************************************************************************************
// Marker
//************************************************************************************************************
Interactions.prototype.showMarker = function(coords, markerStyle) {
    if(markerStyle) this.markerLayer.setStyle(markerStyle);
    this.markerFeature.getGeometry().setCoordinates(coords);
    this.markerLayer.setVisible(true);
};

Interactions.prototype.showMarkerOnLonLat = function(lonlat, markerStyle) {
    this.showMarker(ol.proj.fromLonLat(lonlat), markerStyle);
};

Interactions.prototype.hideMarker = function() {
    this.markerLayer.setVisible(false);
};

export {Interactions as default};