/**
 * Layer object used in {@link Overlays} instance.
 * @memberof Overlays
 * @typedef {Object} LayerGroup
 * @param {string} key - Unique key for this layer group
 * @param {ol.Layer[]} layers - Array of layers in this layer group.
 * @param {callback} [onChange] - On layer visible change listener.
 */

/**
 * Constructor.
 * @class
 * @classdesc Groups and handles a set of layers.
 * @constructor
 * @param {MapHandler} parentMapHandler - Parent map handler.
 * @param {string} [groupName="Overlays"] - Label/name for overlay group.
 * @param {boolean} [multipleAllowed=true] - If false, control is radio group, that is, only one selection 
 *        allowed, meaning only one layer at a time from this group can be visible. However, only affects this
 *        one control.
 * @returns {Overlays}
 */
function Overlays(parentMapHandler, groupName, multipleAllowed) {
    var self             = this;
    this.mapHandler      = parentMapHandler;
    this.groupName       = groupName || "Overlays";
    this.overlays        = {};
    this.control         = null;
    this.controlForm     = null;
    this.controlButton   = null;
    this.controlSelect   = null;
    this.multipleAllowed = !!multipleAllowed;
    this.onChange        = null;
    this._bodyListener   = function(evt) {
        if(self.controlSelect && self.control
            && self.controlSelect.isVisible() 
            && evt.target !== self.control 
            && evt.target.closest('.ol-mh-menu-container') !== self.control
        ) {
            self.controlSelect.style["visibility"] = "hidden";
            self.controlButton.classList.remove("active");
        }
    };
}

/**
 * Layer object used in {@link Overlays} instance.
 * @memberof Overlays
 * @typedef {Object} LayerGroup
 * @param {string} key - Unique key for this layer group
 * @param {ol.Layer[]} layers - Array of layers in this layer group.
 */

/**
 * Callback when layer visibility is changed.
 * @memberof Overlays
 * @callback onLayerChange
 * @param {Event} evt - Passed event object, if available.
 * @param {string} key - Unique name for layer group changed.
 * @param {boolean} visible - What visibile was set to.
 */

//************************************************************************************************************
// Adding/removing groups
//************************************************************************************************************
/**
 * Add layer group.
 * @memberof Overlays
 * @param {string} key - Unique name for layer group.
 * @param {string} name - Name/label for layer group.
 * @param {ol.Layer} layer - Layer to include this in layer group.
 * @param {onLayerChange} [onChange] - Optional callback on change.
 */
Overlays.prototype.addLayer = function(key, name, layer, onChange) {
    this.addLayers(key, name, layer, onChange);
};

/**
 * Add layer group.
 * @memberof Overlays
 * @param {string} key - Unique key for layer group.
 * @param {string} name - Name/label for layer group.
 * @param {(ol.Layer|ol.Layer[])} layers - Layer(s) to include this in layer group.
 * @param {onLayerChange} [onChange] - Optional callback on change.
 */
Overlays.prototype.addLayers = function(key, name, layers, onChange) {
    if(key in this.overlays) this.removeLayer(key);
    if(!Array.isArray(layers)) layers = [layers];
    this.overlays[key] = {name: name, layers: layers, onChange: onChange};
    for(var i = 0; i < this.overlays[key].layers.length; ++i) {
        this.overlays[key].layers[i].setVisible(false);
        this.mapHandler.olMap.addLayer(this.overlays[key].layers[i]);
    }
    if(this.controlForm) {
        var checked = this.controlForm.querySelectorAll("input:checked"), 
            selected = [];
        for(var i = 0; i < checked.length; ++i) {
            selected.push(checked[i].value);
        }
        this._populateControl(selected);
    }
};

/**
 * Remove layer group.
 * @memberof Overlays
 * @param {string} key - Unique name for layer group.
 */
Overlays.prototype.removeLayer = function(key) {
    for(var i = 0; i < this.overlays[key].layers.length; ++i) {
        this.mapHandler.olMap.removeLayer(this.overlays[key].layers[i]);
    }
    delete this.overlays[key];
    if(this.controlForm) {
        var checked = this.controlForm.querySelectorAll("input:checked"), 
            selected = [];
        for(var i = 0; i < checked.length; ++i) {
            selected.push(checked[i].value);
        }
        this._populateControl(selected);
    }
};

/**
 * Remove all layer groups.
 * @memberof Overlays
 */
Overlays.prototype.removeAllLayers = function() {
    for(var key in this.overlays) {
        for(var i = 0; i < this.overlays[key].layers.length; ++i) {
            this.mapHandler.olMap.removeLayer(this.overlays[key].layers[i]);
        }
        delete this.overlays[key];
    }
    this._populateControl();
};


//************************************************************************************************************
// Turning on/off layer groups
//************************************************************************************************************
Overlays.prototype.turnOn = function(key, evt) {
    if(key in this.overlays) {
        for(var i = 0; i < this.overlays[key].layers.length; ++i) {
            this.overlays[key].layers[i].setVisible(true);
        }
        if(this.onChange) this.onChange(evt, key, true);
        if(this.overlays[key].onChange) this.overlays[key].onChange(evt, key, true);
        this._updateControl();
    }
};

Overlays.prototype.turnOff = function(key, evt) {
    if(key in this.overlays) {
        for(var i = 0; i < this.overlays[key].layers.length; ++i) {
            this.overlays[key].layers[i].setVisible(false);
        }
        if(this.onChange) this.onChange(evt, key, false);
        if(this.overlays[key].onChange) this.overlays[key].onChange(evt, key, false);
        this._updateControl();
    }
};

Overlays.prototype.toggle = function(key) {
    if(this.isVisible(key)) {
        this.turnOn(key);
    } else {
        this.turnOff(key);
    }
};

/**
 * Set event listener on change of any layer group visibility.
 * @memberof Overlays
 * @param {onLayerChange} callback - Callback when layer visibility is changed.
 */
Overlays.prototype.setOnChange = function(callback) {
    this.onChange = callback;
};


//************************************************************************************************************
// Getting groups or layers
//************************************************************************************************************
/**
 * Get {@link Overlays#LayerGroup}.
 * @memberof Overlays
 * @param {string} key - Unique name for layer group to retrieve.
 * @returns {LayerGroup}
 */
Overlays.prototype.getLayerGroup = function(key) {
    return this.overlays[key] || null;
};

/**
 * Get ol.Layers for a {@link Overlays#LayerGroup}.
 * @memberof Overlays
 * @param {string} key - Unique name for layers to retrieve.
 * @returns {ol.Layer[]}
 */
Overlays.prototype.getLayers = function(key) {
    if(!key in this.overlays) return [];
    var layers = [];
    for(var i = 0; i < this.overlays[key].layers.length; ++i) {
        layers.push(this.overlays[key].layers[i]);
    }
    return layers;
};

/**
 * Get all layers from all {@link Overlays#LayerGroup} instances handled here.
 * @memberof Overlays
 * @returns {ol.Layer[]}
 */
Overlays.prototype.getAllLayers = function() {
    var layers = [];
    for(var key in this.overlays) {
        for(var i = 0; i < this.overlays[key].layers.length; ++i) {
            layers.push(this.overlays[key].layers[i]);
        }
    }
    return layers;
};

/**
 * Check visibility of a {@link Overlays#LayerGroup}.
 * @memberof Overlays
 * @param {string} key - Unique name for layer group.
 * @param {number} [index=0] - The layer index for the {@link Overlays#LayerGroup} to check. By default just 
 *        checks the first layer, assuming all are handled together to ensure same state. Option to check by 
 *        specific index here to check if that is not the case.
 * @returns {boolean} Whether it is visible.
 */
Overlays.prototype.isVisible = function(key, index) {
    if(!key in this.overlays) return false;
    if(typeof index === "undefined" || !~index) index = 0;
    return this.overlays[key].layers[index].getVisible();
};

/**
 * Check opacity of a {@link Overlays#LayerGroup}.
 * @memberof Overlays
 * @param {string} key - Unique name for layer group.
 * @param {number} [index=0] - The layer index for the {@link Overlays#LayerGroup} to check. By default just 
 *        checks the first layer, assuming all are handled together to ensure same state. Option to check by 
 *        specific index here to check if that is not the case.
 * @returns {number} Opacity.
 */
Overlays.prototype.getOpacity = function(key, index) {
    if(!key in this.overlays) return false;
    if(typeof index === "undefined" || !~index) index = 0;
    return this.overlays[key].layers[index].getOpacity();
};

/**
 * Set opacity of a {@link Overlays#LayerGroup}.
 * @memberof Overlays
 * @param {string} key - Unique name for layer group.
 * @param {number} [index] - The layer index for the {@link Overlays#LayerGroup} to set. By default sets 
 *        opacity for all layers in group if null or undefined (or non-valid index such as -1), but option 
 *        here to set for only one specific layer in the group.
 * @param {number} - Opacity.
 */
Overlays.prototype.setOpacity = function(key, index, opacity) {
    if(!key in this.overlays) return false;
    if(typeof index === "undefined" || index === null || !~index) {
        for(var i = 0; i <  this.overlays[key].layers.length; ++i) {
            this.overlays[key].layers[i].setOpacity(opacity);
        }
    } else {
        this.overlays[key].layers[index || 0].setOpacity(opacity);
    }
};


//************************************************************************************************************
// Controls
//************************************************************************************************************
Overlays.prototype.removeControl = function() {
    if(this.control) {
        document.body.removeEventListener(this._bodyListener);
        this.control.remove();
        this.control = null;
        this.controlForm = null;
        this.controlSelect = null;
        this.controlButton = null;
    }
};

Overlays.prototype.addControl = function(container, direction) {
    // get normal Element object
    if(typeof jQuery !== "undefined" && container instanceof jQuery) {
        container = container[0];
    } else if(typeof container === "string") {
        container = document.querySelector(container);
    }
    
    this.control = document.createElement("div"); 
    this.control.className = 'ol-mh-menu-container';
    container.append(this.control);

    this.controlButton = document.createElement("div"); 
    this.controlButton.className = 'btn ol-mh-menu-button';
    this.controlButton.innerHTML = this.groupName;
    this.control.append(this.controlButton);

    this.controlSelect = document.createElement("div"); 
    this.controlSelect.className = 'ol-mh-menu';
    this.controlSelect.style["visibility"] = "hidden";
    this.control.append(this.controlSelect);
    if(direction === "left") {
        this.controlSelect.classList.add("force-left");
    }

    this.controlForm = document.createElement("form"); 
    this.controlSelect.append(this.controlForm);
    
    var self = this;
    // button toggles
    this.controlButton.addEventListener('click', function() {
        if(self.controlSelect.isVisible()) {
            self.controlSelect.style["visibility"] = "hidden";
            this.classList.remove("active");
        } else {
            self.controlSelect.style["visibility"] = "";
            this.classList.add("active");
        }
    });
    // on clicking outside if open
    document.body.addEventListener('click', this._bodyListener);

    // form change listeners
    this.controlForm.addEventListener('change', function() {
        var checked = self.controlForm.querySelectorAll("input:checked"), 
            selected = [];
        for(var i = 0; i < checked.length; ++i) {
            selected.push(checked[i].value);
        }
        for(var key in self.overlays) {
            var visible = ~selected.indexOf(key);
            for(var i = 0; i < self.overlays[key].layers.length; ++i) {
                self.overlays[key].layers[i].setVisible(visible);
            }
        }
    });

    this._populateControl("none");
};

Overlays.prototype._populateControl = function(selected) {
    if(!this.controlForm) return;

    if(!Array.isArray(selected)) selected = [selected]

    var noneInput = null;
    if(!this.multipleAllowed) {
        var noneInput = document.createElement("input"); 
        noneInput.setAttributes({
            'type':  'radio', 
            'name':  'ol-mh-overlay-select', 
            'value': 'none'
        });
        this.controlForm.append(noneInput);
        this.controlForm.append(" None");
        this.controlForm.append(document.createElement("br"));
    }

    var selectFound = false;
    for(var key in this.overlays) {
        var input = document.createElement("input"); 
        input.setAttributes({
            'type':  this.multipleAllowed ? 'checkbox' : 'radio', 
            'name':  'ol-mh-overlay-select', 
            'value': key
        });
        if((this.multipleAllowed || !selectFound) && ~selected.indexOf(key)) {
            input.checked = true;
            selectFound = true;
        }
        this.controlForm.append(input);
        this.controlForm.append(" " + this.overlays[key].name);
        this.controlForm.append(document.createElement("br"));
    }

    if(noneInput && !selectFound) noneInput.checked = true;
};

Overlays.prototype._updateControl = function() {
    if(!this.controlForm) return;
    var inputs = this.controlForm.querySelectorAll("input"), 
        noneInput = null, 
        selectFound = false;
    for(var i = 0; i < inputs.length; ++i) {
        var layerKey = inputs[i].getAttribute("value");
        if(layerKey === "none") {
            noneInput = inputs[i];
        } else if(this.multipleAllowed || !selectFound) {
            if(layerKey in this.overlays && this.overlays[layerKey][0].isVisible()) {
                selectFound = true;
                inputs[i].checked = true;
            } else {
                inputs[i].checked = false;
            }
        } else {
            inputs[i].checked = false;
        }
    }
    if(noneInput) noneInput.checked = !selectFound;
};

export {Overlays as default};
