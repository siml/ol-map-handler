import MapHandler from "./map-handler.js";
import Basemap from "./basemap";
import Overlays from "./overlays";

// Nest component subclasses
MapHandler.Basemap = Basemap;
MapHandler.Overlays = Overlays;

export {MapHandler as default};