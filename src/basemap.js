/**
 * Constructor
 * @class
 * @classdef Basemap handler.
 * @constructor
 * @param {Object[]} inputBaseLayers
 * @param {String} inputBaseLayers[].name - Name of the base layer.
 * @param {ol.layer.Tile} inputBaseLayers[].base - Tile layer of this basemap.
 * @param {ol.layer.Tile} [inputBaseLayers[].reference] - Optional reference layer that can be turned on/off 
 *        on top of the main layer.
 * @param {number} [basemapSelectIndex=0] - Index of selected basemap to initialize with.
 *        dropdown select box).
 * @returns {Basemap}
 */
function Basemap(inputBaseLayers, basemapSelectIndex) {
    var self                  = this;
    this.attributionElem      = null;
    this.baseLayers           = inputBaseLayers;
    this.baseLayersArray      = [];
    this.referenceLayersArray = [];
    this.baseLayersGroup      = null;
    this.drawReferenceLayer   = true;
    this._onChange            = null;
    this.control              = null;
    this.controlForm          = null;
    this.controlSelect        = null;
    this.controlButton        = null;
    this._bodyListener        = function(evt) {
        if(self.controlSelect && self.control
            && self.controlSelect.isVisible() 
            && evt.target !== self.control 
            && evt.target.closest('.ol-mh-menu-container') !== self.control
        ) {
            self.controlSelect.style["visibility"] = "hidden";
            self.controlButton.classList.remove("active");
        }
    };

    for(var i = 0; i < this.baseLayers.length; i++) {
        this.baseLayersArray.push(this.baseLayers[i].layers.base);
        if(this.baseLayers[i].layers.reference) {
            this.referenceLayersArray.push(this.baseLayers[i].layers.reference);
        }
    }
    this.changeBasemap(basemapSelectIndex);
    this.baseLayersGroup = new ol.layer.Group({
        zIndex: 0, 
        layers: this.baseLayersArray.concat(this.referenceLayersArray)
    });
};

Basemap.prototype.removeControl = function() {
    if(this.control) {
        document.body.removeEventListener(this._bodyListener);
        this.control.remove();
        this.control = null;
        this.controlForm = null;
        this.controlSelect = null;
        this.controlButton = null;
    }
};

/**
 * Adds basemap control to given element.
 * @memberof Basemap
 * @param {(Element|jQuery|string)} container
 */
Basemap.prototype.addControl = function(container, direction) {
    // get normal Element object
    if(typeof jQuery !== "undefined" && container instanceof jQuery) {
        container = container[0];
    } else if(typeof container === "string") {
        container = document.querySelector(container);
    }

    this.control = document.createElement("div"); 
    this.control.className = 'ol-mh-menu-container';
    container.append(this.control);

    this.controlButton = document.createElement("div"); 
    this.controlButton.className = 'btn ol-mh-menu-button';
    this.controlButton.innerHTML = "Basemaps";
    this.control.append(this.controlButton);

    this.controlSelect = document.createElement("div"); 
    this.controlSelect.className = 'ol-mh-menu';
    this.controlSelect.style["visibility"] = "hidden";
    this.control.append(this.controlSelect);
    if(direction === "left") {
        this.controlSelect.classList.add("force-left");
    }

    this.controlForm = document.createElement("form"); 
    this.controlSelect.append(this.controlForm);

    // populate form
    for(var i = 0; i < this.baseLayers.length; i++) {
        var input = document.createElement("input"); 
        input.setAttributes({
            'type':  'radio', 
            'name':  'ol-mh-basemap-select', 
            'value': i
        });
        input.checked = this.baseLayers[i].layers.base.getVisible();
        this.controlForm.append(input);
        this.controlForm.append(" " + this.baseLayers[i].name);
        this.controlForm.append(document.createElement("br"));
    }

    var self = this;
    // button toggles
    this.controlButton.addEventListener('click', function() {
        if(self.controlSelect.isVisible()) {
            self.controlSelect.style["visibility"] = "hidden";
            this.classList.remove("active");
        } else {
            self.controlSelect.style["visibility"] = "";
            this.classList.add("active");
        }
    });
    // on clicking outside if open
    document.body.addEventListener('click', this._bodyListener);

    // form change listeners
    this.controlForm.addEventListener('change', function() {
        self.changeBasemap(
            self.controlForm.querySelector("input:checked").value, 
            false, 
            !self.drawReferenceLayer
        );
    });
};

Basemap.prototype.setAttributionElement = function(elem) {
    this.removeAttributionElement();
    // get normal Element object
    if(typeof jQuery !== "undefined" && elem instanceof jQuery) {
        elem = elem[0];
    } else if(typeof elem === "string") {
        elem = document.querySelector(elem);
    }
    this.attributionElem = elem;
    this.updateAttribution();
};

Basemap.prototype.removeAttributionElement = function() {
    if(this.attributionElem) {
        this.attributionElem.style["visbility"] = "";
        this.attributionElem.innerHTML = "";
        this.attributionElem = null;
    }
};

Basemap.prototype.updateAttribution = function(layerIndex) {
    if(!this.attributionElem) return;
    if(layerIndex === undefined || layerIndex === null) {
        layerIndex = this.lastLayerIndex;
    }
    if(layerIndex < 0) {
        this.attributionElem.style["visbility"] = "hidden";
    } else {
        var attributions = this.baseLayers[layerIndex].layers.base.getSource().getAttributions();
        if(attributions && attributions.length > 0) {
            this.attributionElem.innerHTML = attributions[0].getHTML();
            this.attributionElem.style["visbility"] = "";
        } else {
            this.attributionElem.style["visbility"] = "hidden";
        }
    }
};

Basemap.prototype.onChange = function(callback) {
    this._onChange = callback;
};

/**
 * Change basemap using index.
 * @memberof Basemap
 * @param {Number} layerIndex - Base layer index.
 * @param {Boolean} [turnOff] - If true, turns all baselayers off.
 * @param {Boolean} [skipReference] - If true, does not load the associated reference layer. If null or 
 *        undefined, determines via the last turnOnReference() or turnOffReference() call.
 * @param {Boolean} [referenceOnly] - If true, applied to the reference layer (e.g. some baselayers are a 
 *        combination of the image layer and a reference layer on top).
 */
Basemap.prototype.changeBasemap = function(layerIndex, turnOff, skipReference, referenceOnly) {
    if(turnOff) {
        layerIndex = -1;
    } else {
        layerIndex = parseInt(layerIndex);
        if(!layerIndex || layerIndex < 0 || layerIndex >= this.baseLayers.length) { 
            layerIndex = 0; 
        }
        this.lastLayerIndex = layerIndex;
    }
    skipReference = skipReference !== undefined && skipReference !== null ? skipReference : !this.drawReferenceLayer;
    referenceOnly = skipReference ? false : referenceOnly;
    for(var i = 0; i < this.baseLayers.length; i++) {
        if(!referenceOnly) {
            this.baseLayers[i].layers.base.setVisible(layerIndex === i);
        }
        if(this.baseLayers[i].layers.reference) {
            this.baseLayers[i].layers.reference.setVisible(!skipReference && layerIndex === i);
        }
    }
    if(!referenceOnly) {
        this.updateAttribution(layerIndex);
    }
    if(this._onChange) {
        this._onChange(layerIndex);
    }
};

/**
 * Turn on the last baselayer that was on.
 * @memberof Basemap
 */
Basemap.prototype.turnOn = function() {
    this.changeBasemap(this.lastLayerIndex);
};

/**
 * Turn off all baselayers.
 * @memberof Basemap
 */
Basemap.prototype.turnOff = function() {
    this.changeBasemap(-1, true);
};

/**
 * Turn on the reference layer of the last baselayer that was on.
 * @memberof Basemap
 */
Basemap.prototype.turnOnReference = function() {
    this.drawReferenceLayer = true;
    this.changeBasemap(this.lastLayerIndex, false, false, true);
};

/**
 * Turn off the reference layer.
 * @memberof Basemap
 */
Basemap.prototype.turnOffReference = function() {
    this.drawReferenceLayer = false;
    this.changeBasemap(-1, true, false, true);
};

export {Basemap as default};