import common from "./lib/common/dist/common.min";
import {InteractionHandler} from "./lib/interaction-handler/interaction-handler.min";
import Interactions from "./interactions/interactions";
/**
 * Constructor for MapHandler.
 * @class
 * @classdef Map handler.
 * @constructor
 * @param {Element|jQuery|String} mapContainer - Container to append map to. May be DOM Element, jQuery 
 *        selection, or selector string.
 * @param {Object} [mapOptions]
 * @param {Number} [mapOptions.initZoomLevel=6] - Optional, init zoom level.
 * @param {Number[]} [mapOptions.center=[-120, 37.5]] - Optional, init map center.
 * @param {Number[]} [mapOptions.minMaxZoom=[1, 28]] - Optional,restricted min/max zoom levels.
 * @param {Number[]} [mapOptions.extent=[-260, -75, 160, 75]] - Optional, restricted map extent.
 * @returns {MapHandler}
 */
function MapHandler(mapContainer, mapOptions) {
    this.mapContainer = mapContainer;
    // get normal Element object
    if(typeof jQuery !== "undefined" && this.mapContainer instanceof jQuery) {
        this.mapContainer = this.mapContainer[0];
    } else if(typeof this.mapContainer === "string") {
        this.mapContainer = document.querySelector(this.mapContainer);
    }

    // hard-coded parameters
    this.mapViewClass         = "ol-mh-map-view";
    this.mapControlsId        = "ol-mh-map-controls";
    this.epsgWebMercator      = 'EPSG:3857',  // web mercator wgs84
    this.epsgWgs84            = 'EPSG:4326';  // assumed coordinate-system for any incoming data
    this.projWebMercator      = ol.proj.get(this.epsgWebMercator);
    this.projWgs84            = ol.proj.get(this.epsgWgs84);
    // parameters to be filled in later at mapInit() or elsewhere before init is completed
    this.olMap                = null;  // ol.Map instance
    this.mapViewElem          = null;  // map container element
    this.controlElem          = null;  // layer controls element
    this.controlDir           = null;  // direction of layer control popup
    this.resetViewCenter      = null;  // default/reset view stuff below
    this.resetViewExtent      = null;
    this.resetViewZoom        = null;
    this.resetViewMinMaxZoom  = null;
    // OL overview map
    this.overviewMap          = null;
    // layer handlers
    this.overlays             = {};
    this.basemapModule        = null;
    // interaction handlers
    this.interactionHandler   = null;
    this.interactions         = null;
    // map listeners
    this._olMapListeners      = [];

    // init map element
    if(!mapOptions)               mapOptions = {};
    if(!mapOptions.initZoomLevel) mapOptions.initZoomLevel = 6;
    if(!mapOptions.minMaxZoom)    mapOptions.minMaxZoom = [1, 28]; //[6, 14];
    if(!mapOptions.center)        mapOptions.center = [-121, 37.5];
    if(!mapOptions.extent)        mapOptions.extent = [-260, -75, 160, 75]; //[-124, 32.5, -114, 43];

    // save view settings in case of reset
    this.resetViewCenter     = ol.proj.fromLonLat(mapOptions.center, this.projWebMercator);
    this.resetViewExtent     = ol.proj.transformExtent(mapOptions.extent, this.projWgs84, this.projWebMercator);
    this.resetViewZoom       = mapOptions.initZoomLevel;
    this.resetViewMinMaxZoom = mapOptions.minMaxZoom;
    
    // get normal Element object
    if(typeof jQuery !== "undefined" && this.mapContainer instanceof jQuery) {
        this.mapContainer = this.mapContainer[0];
    } else if(typeof this.mapContainer === "string") {
        this.mapContainer = document.querySelector(this.mapContainer);
    }

    // create actual map div
    this.mapViewElem = document.createElement("div");
    this.mapViewElem.className = this.mapViewClass;
    this.mapContainer.prepend(this.mapViewElem);
    // create map and view (take off default zoom control, we'll place manually later)
    this.olMap = new ol.Map({ target: this.mapViewElem, controls: [] });
    this.setMapView();
    // grabbing cursor functionality since it's not default to open layers 3
    common.addGrabCursorFunctionality(this.mapViewElem);
    
    // initalize interactions
    this.interactionHandler = new InteractionHandler(this.olMap);
    this.interactions = new Interactions(this, this.interactionHandler);
    this.interactions._init();
    this.interactions.hover.activate();
    this.interactions.click.activate();
    
    // default webmerc tilegrid
    var zoomLevels       = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], 
        projectionExtent = this.projWebMercator.getExtent(), 
        tileSize         = ol.extent.getWidth(projectionExtent) / 256;
    this.defaultTileGrid = new ol.tilegrid.WMTS({
                               origin: ol.extent.getTopLeft(projectionExtent),
                               resolutions: zoomLevels.map(function(z) { return tileSize / Math.pow(2,z); }),
                               matrixIds: zoomLevels
                           });
};

/**
 * Safely remove MapHandler. Ensures no listeners are still attached and variables are best left for garbage 
 * collection.
 * @memberof MapHandler
 */
MapHandler.prototype.exit = function() {
    this.removeBasemapModule();
    for(var key in this.overlays) {
        this.overlays[key].removeAllLayers();
        this.overlays[key].removeControl();
        delete this.overlays[key];
    }
    for(var i = 0; i < this._olMapListeners.length; ++i) {
        this._un(this._olMapListeners[i][0], this._olMapListeners[i][1]);
    }
    this.olMapListeners = null;
    this.olMap.setTarget(null);
    this.olMap = null;
};


//************************************************************************************************************
// Basic Map Functions and Misc Functions
//************************************************************************************************************
/**
 * Zoom in one level.
 * @memberof MapHandler
 */
MapHandler.prototype.zoomIn = function() {
    var view = this.olMap.getView();
    view.setZoom(view.getZoom()+1);
};

/**
 * Zoom out one level.
 * @memberof MapHandler
 */
MapHandler.prototype.zoomOut = function() {
    var view = this.olMap.getView();
    view.setZoom(view.getZoom()-1);
};

/**
 * Set (or reset) map view and properties of map view.
 * @memberof MapHandler
 * @param {Number[]} [center] - Map center, optional, if not provided uses default from map init.
 * @param {Number} [zoom] - Zoom level, optional, if not provided uses default from map init.
 * @param {Number[]} [extent] - Map extent, optional, if not provided uses default from map init.
 * @param {Number[]} [minMaxZoom] - Min and max zoom levels, optional, if not provided uses default from 
 *        map init.
 */
MapHandler.prototype.setMapView = function(center, zoom, extent, minMaxZoom) {
    if(!center)     center = this.resetViewCenter;
    if(!zoom)       zoom = this.resetViewZoom;
    if(!extent)     extent = this.resetViewExtent;
    if(!minMaxZoom) minMaxZoom = this.resetViewMinMaxZoom;
    this.olMap.setView(
        new ol.View({
            center: center,
            zoom: zoom,
            minZoom: minMaxZoom[0],
            maxZoom: minMaxZoom[1],
            extent: extent
        })
    );
};

/**
 * Get center coordinates of current map view.
 * @memberof MapHandler
 * @returns {Number[]} - Coordinates in Web Mercator.
 */
MapHandler.prototype.getMapCenterCoordinates = function() {
    return this.olMap.getView().getCenter();
};

/**
 * Get map coordinates of a event.
 * @memberof MapHandler
 * @param {Event} - Event, obviously one that would be related to map coordinates, e.g. mouse click or 
*         mousemove.
 * @returns {Number[]} - Coordinates in Web Mercator.
 */
MapHandler.prototype.getEventCoordinates = function(evt) {
    return this.olMap.getEventCoordinate(evt.originalEvent || evt);
};

/**
 * Quick conversion of coordinates from Web Mercator to geographic.
 * @memberof MapHandler
 * @param {Number[]} - Web Mercator coordinates.
 * @returns {Number[]} - Coordinates in longitude, latitude.
 */
MapHandler.prototype.coordinatesToLonLat = function(coords) {
    return ol.proj.toLonLat(coords, this.projWebMercator);
};

/**
 * Quick conversion of coordinates from geographic to Web Mercator.
 * @memberof MapHandler
 * @param {Number[]} - Longitude, latitude coordinates.
 * @returns {Number[]} - Coordinates in Web Mercator.
 */
MapHandler.prototype.coordinatesFromLonLat = function(coords) {
    return ol.proj.fromLonLat(coords, this.projWebMercator);
};

/**
 * Check if given coordinates are within current map view.
 * @memberof MapHandler
 * @param {Number[]} - Web Mercator coordinates.
 * @returns {Boolean} True if coordinates within map view extent.
 */
MapHandler.prototype.checkWithinExtent = function(coords) {
    if(coords[0] < this.resetViewExtent[0] || coords[0] > this.resetViewExtent[2]) return false;
    if(coords[1] < this.resetViewExtent[1] || coords[1] > this.resetViewExtent[3]) return false;
    return true;
};


//************************************************************************************************************
// Layer handlers
//************************************************************************************************************
/**
 * Attach a basemap module.
 * @memberof MapHandler
 * @param {MapHandler.Basemap} basemapModule - Basemap module
 */
MapHandler.prototype.setBasemapModule = function(basemapModule) {
    this.removeBasemapModule();
    this.basemapModule = basemapModule;
    this.olMap.setLayerGroup(this.basemapModule.baseLayersGroup);
    this.basemapModule.baseLayersGroup.setZIndex(0);
};

/**
 * Remove basemap module.
 * @memberof MapHandler
 */
MapHandler.prototype.removeBasemapModule = function() {
    if(this.basemapModule) {
        this.basemapModule.removeControl();
        this.basemapModule.removeAttributionElement();
        for(var i = 0; i < this.basemapModule.baseLayersArray.length; ++i) {
            this.olMap.removeLayer(this.basemapModule.baseLayersArray[i]);
        }
        for(var i = 0; i < this.basemapModule.referenceLayersArray.length; ++i) {
            this.olMap.removeLayer(this.basemapModule.referenceLayersArray[i]);
        }
        this.basemapModule = null;
    }
};

/**
 * Create overlay group. Overlay groups must be given a unique key (or previous overlay group with that key 
 * will be replaced). However a separate display name (which doesn't have to be unique), may be added.
 * @memberof MapHandler
 * @param {String} groupName - Unique group name key.
 * @param {String} [displayName] - Optional display name.
 * @param {Boolean} [allowMultiple] - If true, allows multiple selections, but if false, only one layer may be
 *        visible at one time.
 */
MapHandler.prototype.createOverlayGroup = function(groupName, displayName, allowMultiple) {
    if(groupName in this.overlays) this.removeOverlayGroup(groupName);
    this.overlays[groupName] = new MapHandler.Overlays(this, displayName || groupName, allowMultiple);
};

/**
 * Add a layer to an overlay group. Layer key must be unique within the overlay group, but doesn't have to be 
 * unique across all overlay groups.
 * @memberof MapHandler
 * @param {String} groupName - Unique group name key.
 * @param {String} layerKey - Unique layer name key.
 * @param {String} layerName - Layer display name.
 * @param {ol.layer[]|ol.layer} layers - Layer or layers.
 */
MapHandler.prototype.addToOverlayGroup = function(groupName, layerKey, layerName, layers) {
    this.overlays[groupName].addLayers(layerKey, layerName, layers);
};

/**
 * Turn on an overlay layer.
 * @memberof MapHandler
 * @param {String} groupName - Unique group name key.
 */
MapHandler.prototype.turnOnOverlay = function(groupName, layerKey) {
    this.overlays[groupName].turnOn(layerKey);
};

/**
 * Turn off an overlay layer.
 * @memberof MapHandler
 * @param {String} groupName - Unique group name key.
 */
MapHandler.prototype.turnOffOverlay = function(groupName, layerKey) {
    this.overlays[groupName].turnOff(layerKey);
};

/**
 * Remove layer from overlay group.
 * @memberof MapHandler
 * @param {String} groupName - Unique group name key.
 * @param {String} layerKey - Unique layer name key.
 */
MapHandler.prototype.removeFromOverlayGroup = function(groupName, layerKey) {
    if(groupName in this.overlays) {
        this.overlays[groupName].removeLayer(layerKey);
    }
};

/**
 * Remove overlay group by unique key (or optionally, provide the actual Overlay instance).
 * @memberof MapHandler
 * @param {String|MapHandler.Overlay} groupName - Unique group name key.
 */
MapHandler.prototype.removeOverlayGroup = function(groupName) {
    if(groupName instanceof Overlays) {
        for(var key in this.overlays) {
            if(this.overlays[key] === groupName) {
                this.overlays[key].removeAllLayers();
                this.overlays[key].removeControl();
                delete this.overlays[key];
                break;
            }
        }
    } else if(groupName in this.overlays) {
        this.overlays[groupName].removeAllLayers();
        this.overlays[groupName].removeControl();
        delete this.overlays[groupName];
    }
};


//************************************************************************************************************
// Specific event listeners
//************************************************************************************************************
/**
 * Add event listener. By default on the ol.Map instance. However you can prefix "view:" to add listener to 
 * the ol.View instance. An additional custom type is "basemap" to attach a listener to basemap change.
 * @memberof MapHandler
 * @param {String} type - The type of event. Attached to ol.Map, or ol.View if prefixed with "view:", or 
 *        "basemap" for listener on basemap change.
 * @param {Callback} listener - Event listener to attach.
 */
MapHandler.prototype.on = function(type, listener) {
    this._olMapListeners.push([type, listener]);
    switch(type.split(":")[0]) {
        case "view":
            this.olMap.getView().on(type.split(":")[1], listener);
            break;
        case "basemap":
            this.basemapModule.onChange(listener);
            break;
        default:
            this.olMap.on(type, listener);
    }
};

/** 
 * Remove event listener.
 * @memberof MapHandler
 * @param {String} type - The type of event. Attached to ol.Map, or ol.View if prefixed with "view:", or 
 *        "basemap" for listener on basemap change.
 * @param {Callback} listener - Event listener to detach.
 */
MapHandler.prototype.un = function(type, listener) {
    for(var i = 0; i < this._olMapListeners.length; ++i) {
        if(type === this._olMapListeners[i][0] && listener === this._olMapListeners[i][1]) {
            var remove = this._olMapListeners.splice(i, 1)[0];
            this._unt(remove[0], remove[1]);
            break;
        }
    }
};

MapHandler.prototype._un = function(type, listener) {
    if(type.split(":")[0] === "view") {
        this.olMap.getView().un(type.split(":")[1], listener);
    } else {
        this.olMap.un(type, listener);
    }
};


//************************************************************************************************************
// Overview Map Functionality
//************************************************************************************************************
MapHandler.prototype.showOverviewMap = function(layers, view) {
    if(!this.overviewMap) {
        var omOptions = {
            collapsed: false, 
            collapsible: false
        };
        if(layers) { omOptions.layers = layers; }
        if(view) { omOptions.view = view; }
        this.overviewMap = new ol.control.OverviewMap(omOptions);
        this.olMap.addControl(this.overviewMap);
    }
};

MapHandler.prototype.hideOverviewMap = function() {
    if(this.overviewMap) {
        this.olMap.removeControl(this.overviewMap);
        this.overviewMap = null;
    }
};


//************************************************************************************************************
// Element handlers
//************************************************************************************************************
/**
 * Set an element to append layer controls too. That is, the controls for basemaps and other overlay groups. 
 * If controls have already been added somewhere, that one will be removed.
 * @memberof MapHandler
 * @param {(Element|jQuery|String)} container - DOM Element, jQuery selection, or string selector for element 
 *        to append controls to.
 * @param {String} [direction="right"] - Direction for menu to appear. Defaults to right, but may also specify
 *        menu opening on the left side.
 */
MapHandler.prototype.addControl = function(container, direction) {
    // get normal Element object
    if(typeof jQuery !== "undefined" && container instanceof jQuery) {
        container = container[0];
    } else if(typeof container === "string") {
        container = document.querySelector(container);
    }
    this.controlElem = container;
    this.controlDir  = direction || "right";
    this.renderControl();
};

/**
 * Remove layer controls.
 * @memberof MapHandler
 */
MapHandler.prototype.removeControl = function() {
    if(!this.controlElem) return;
    if(this.basemapModule) this.basemapModule.removeControl();
    for(var groupName in this.overlays) {
        this.overlays[groupName].removeControl();
    }
};

/**
 * Rerender layer controls.
 * @memberof MapHandler
 */
MapHandler.prototype.renderControl = function() {
    if(!this.controlElem) return;
    this.removeControl();
    if(this.basemapModule) this.basemapModule.addControl(this.controlElem, this.controlDir);
    for(var groupName in this.overlays) {
        this.overlays[groupName].addControl(this.controlElem, this.controlDir);
    }
};

/**
 * Specify element to update basemap attributions.
 * @memberof MapHandler
 * @param {(Element|jQuery|String)} container - DOM Element, jQuery selection, or string selector for element 
 *        to append attribution element to.
 */
MapHandler.prototype.setAttributionElement = function(container) {
    if(!container) return this.removeAttributionElement();
    if(this.basemapModule) this.basemapModule.setAttributionElement(container);
}; 

/**
 * Remove element for basemap attributions.
 * @memberof MapHandler
 */
MapHandler.prototype.removeAttributionElement = function() {
    if(this.basemapModule) this.basemapModule.removeAttributionElement();
};


export {MapHandler as default};