const webpack = require('webpack'), 
      path    = require('path')
      MiniCssExtractPlugin    = require("mini-css-extract-plugin"), 
      OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
module.exports = {
    entry: './src/entry.js', 
    mode: 'production', 
    output: {
        library: "MapHandler", 
        libraryTarget: 'this', 
        libraryExport: 'default', 
        path: path.resolve(__dirname, "dist"),
        filename: 'ol-map-handler.min.js'
    },
    module: {
        rules: [
            {
                test:    /\.js$/,
                exclude: /(node_modules)/,
                loader:  'babel-loader', 
                query: {
                    presets: ['@babel/preset-env']
                }
            }, 
            {
                test: /\.css$/,
                use:  [
                    MiniCssExtractPlugin.loader, 
                    "css-loader"
                ]
            }
        ]
    }, 
    optimization: {
        concatenateModules: true, 
        minimize: true
    }, 
    plugins: [
        new MiniCssExtractPlugin({
            filename: "ol-map-handler.min.css"
        })
    ]
};