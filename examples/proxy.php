<?php
$get = isset($_GET['url']);
$url = $_REQUEST['url'];
$data = array();
foreach($_REQUEST as $key => $val) {
    if($key === "url") { continue; }
    if($get) {
        $url .= "&$key=$val";
    } else {
        $data[$key] = $val;
    }
}
if($get) {
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'GET'
        )
    );
} else {
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
}
$context = stream_context_create($options);
echo file_get_contents($url, false, $context);